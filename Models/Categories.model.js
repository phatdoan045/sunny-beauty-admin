var mongoose = require("mongoose");

var categorySchema = new mongoose.Schema(
  {
    parentId: {
      type: mongoose.Types.ObjectId,
      require: false,
      default: null,
    },
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
      required: false,
    },
  },
  {
    versionKey: false,
  }
);
var categorySchema = mongoose.model("categogies", categorySchema, "categogies");
module.exports = categorySchema;
