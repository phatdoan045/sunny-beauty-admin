var mongoose = require("mongoose");
var productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    englishname: {
      type: String,
      required: true,
    },
    categoryId: {
        type: mongoose.Types.ObjectId,
        required: true,
    },
    description: {
      type: String,
      required: true,
    },
    origin: {
      type: String,
      required: false,
      default: null,
    },
    brandId: {
        type: mongoose.Types.ObjectId,
        required: true,
    },
    inStock: {
      type: Number,
      required: true,
    },
    capacity: {
      type: String,
      required: false,
      default: null,
    },
    color: {
      type: Array,
      required: false,
    },
    importPriceUSD: {
      type: Number,
      required: true,
    },
    importPriceVND: {
      type: Number,
      required: true,
    },
    importFee: {
      type: Number,
      required: true,
    },
    retailPrice: {
      type: Number,
      required: true,
    },
    wholeSalePrice: {
      type: Number,
      required: true,
    },
    barcode: {
      type: Number,
      required: true,
    },
    status: {
      type: Number,
      required: true,
    },
    productImage: {
      type: Array,
      required: false,
      default: []
    },
  },
  {
    versionKey: false,
  }
);
var productSchema = mongoose.model("products", productSchema, "products");
module.exports = productSchema;


