var mongoose = require("mongoose");

var BrandsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
    description: {
      type: String,
      required: false,
      default: null,
    },
    brandImage: {
      type: String,
      required: false,
      default: null,
    },
  },
  {
    versionKey: false,
  }
);
var BrandsSchema = mongoose.model("brands", BrandsSchema, "brands");
module.exports = BrandsSchema;
