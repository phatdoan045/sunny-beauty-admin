var mongoose = require("mongoose");
const multer = require("multer");
const path = require("path");
const fs = require("fs");

var products = require("../Models/Product.model");
var categogies = require("../Models/Categories.model");
var brands = require("../Models/Brands.model");

const Notification = require("../Notification/Notification");
const functionUtil = require("../Util/Function.util");

module.exports.getAllProduct = async (req, res) => {
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 1;
  await products.aggregate(
    [
      {
        $skip: (page - 1)*limit,
      },
      {
        $limit: limit,
      },
      {
        $facet: {
          products: [
            {
              $lookup: {
                from: "categogies",
                localField: "categoryId",
                foreignField: "_id",
                as: `categogy`,
              },
            },
            {
              $lookup: {
                from: "brands",
                localField: "brandId",
                foreignField: "_id",
                as: "brand",
              },
            },
            {
              $project: {
                categoryId: 0,
                brandId: 0,
                inStock: 0,
                importPriceUSD: 0,
                importPriceVND: 0,
                importFee: 0,
                cost: 0,
                retailPrice: 0,
                wholeSalePrice: 0,
              },
            },
          ],
        },
      },
    ],
    async (err, productData) => {
      const totalRecords = await products.estimatedDocumentCount();
      let totalPages =  Math.ceil(totalRecords / limit);
      if(totalRecords % limit !== 0 ){
         totalPages=totalPages+1;
      }
      if (err) {
        console.log(err);
        res.json({
          statusCode: 500,
          message: Notification.ERRORDATABASE,
        });
      } else {
        res.json({
          page: page,
          limit: limit,
          totalRecords: totalRecords,
          totalPages: totalPages,
          data: productData,
        });
      }
    }
  );
};

const storageProductImage = multer.diskStorage({
  destination: "./Public/Uploads/ProductImage",
  filename: function (req, file, cb) {
    cb(null, "IMAGE-PRODUCT" + Date.now() + path.extname(file.originalname));
  },
});
const uploadMultipble = multer({
  storage: storageProductImage,
  limits: { fileSize: 1000000 },
}).array("files", 10);

module.exports.deleteProduct = async (req, res) => {
  let { id } = req.params;
  products.findOne({ _id: id }, (err, product) => {
    if (err) {
      console.log(err);
      res.json({
        status: 404,
        message: Notification.NOFOUND,
      });
    } else {
      if (product.productImage) {
        product.productImage.forEach((item) => {
          let imagename = `Public/Uploads/ProductImage/${item}`;
          fs.unlink(imagename, (err) => {
            if (err) {
              return res.json({
                statusCode: 500,
                message: Notification.DELETEIMAGESERVERERROR,
              });
            }
          });
        });
      }
      products.findOneAndRemove({ _id: id }, (err) => {
        if (err) {
          console.log(err);
          res.json({
            statusCode: 404,
            message: Notification.ERRORDATABASE,
          });
        } else {
          res.json({
            statusCode: 200,
            message: Notification.DELETEPRODUCTSUCCESS,
          });
        }
      });
    }
  });
};
module.exports.createProduct = async (req, res) => {
  let arrayproductImageFlag = [];
  await uploadMultipble(req, res, (err) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 405,
        message: Notification.UPLOADIMAGEFAILED,
      });
    } else {
      req.files.map((item) => {
        arrayproductImageFlag.push(item.filename);
      });
      let {
        name,
        englishname,
        categoryId,
        description,
        origin,
        brandId,
        inStock,
        capacity,
        color,
        importPriceUSD,
        importPriceVND,
        importFee,
        cost,
        retailPrice,
        wholeSalePrice,
        barcode,
        status,
      } = req.body;
      brandId = mongoose.Types.ObjectId(brandId);
      categoryId = mongoose.Types.ObjectId(categoryId);
      var product = {
        name,
        englishname,
        categoryId,
        description,
        origin,
        brandId,
        inStock,
        capacity,
        color,
        importPriceUSD,
        importPriceVND,
        importFee,
        cost,
        retailPrice,
        wholeSalePrice,
        barcode,
        status,
        productImage: arrayproductImageFlag,
      };
      let newProduct = new products(product);
      newProduct.save((err) => {
        if (err) {
          console.log(err);
          res.json({
            message: Notification.ERRORDATABASE,
            result: false,
            statusCode: 500,
          });
        } else {
          res.json({
            message: Notification.CREATEPRODUCTSUCCESS,
            statusCode: 200,
          });
        }
      });
    }
  });
};

module.exports.EditProduct = async (req, res) => {
  await uploadMultipble(req, res, (err) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 405,
        message: Notification.UPLOADIMAGEFAILED,
      });
    } else {
      let {
        id,
        name,
        englishname,
        categoryId,
        description,
        origin,
        brandId,
        inStock,
        capacity,
        color,
        importPriceUSD,
        importPriceVND,
        importFee,
        cost,
        retailPrice,
        wholeSalePrice,
        barcode,
        status,
        productImage,
        arrDeleteImageProduct,
      } = req.body;
      if (req.files) {
        if(!productImage){
          productImage=[];
        }
        req.files.map((item) => {
          productImage.push(item.filename);
        });
      }
      if (arrDeleteImageProduct) {
        arrDeleteImageProduct.map((item) => {
          functionUtil.DeleteItemArray(productImage, item);
          let imagename = `Public/Uploads/ProductImage/${item}`;
          fs.unlink(imagename, (err) => {
            if (err) {
              return res.json({
                statusCode: 500,
                message: Notification.DELETEIMAGESERVERERROR,
              });
            }
          });
        });
      }
      products.findByIdAndUpdate({ "_id": id }, { 
        id,
        name,
        englishname,
        categoryId,
        description,
        origin,
        brandId,
        inStock,
        capacity,
        color,
        importPriceUSD,
        importPriceVND,
        importFee,
        cost,
        retailPrice,
        wholeSalePrice,
        barcode,
        status,
        productImage }, (err) => {
        if (err) {
          console.log(err);
          res.json({
            statusCode: 500,
            message: Notification.ERRORDATABASE,
          });
        }
        else{
          res.json({
            statusCode:200,
            message:Notification.EDIT_PRODUCT_SUCCESS
          })
        }
      });      
    }
  });
};

module.exports.getImageProduct = (req, res) => {
  let imagename = "Public/Uploads/ProductImage/" + req.params.image;
  fs.readFile(imagename, (err, ImageData) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 500,
        message: Notification.GETIMAGEFAILED,
      });
    } else {
      res.setHeader("Content-Type", "image/jpeg");
      res.end(ImageData);
    }
  });
};
