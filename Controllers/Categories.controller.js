var category = require("../Models/Categories.model");
const Notification = require("../Notification/Notification");

module.exports.getCategoryAll = (req, res) => {
  category.find((err, category) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 500,
        message: Notification.ERRORDATABASE,
      });
    } else {
      res.json({
        data: category,
      });
    }
  });
};

module.exports.getCategoryLimit = async (req, res) => {
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 1;
  await category.aggregate(
    [
      {
        $skip: (page - 1) * limit,
      },
      {
        $limit: limit,
      },
    ],
    async (err, categoryData) => {
      const totalRecords = await category.estimatedDocumentCount();
      if (err) {
        console.log(err);
        res.json({
          statusCode: 500,
          message: Notification.ERRORDATABASE,
        });
      } else {
        res.json({
          page: page,
          limit: limit,
          totalRecords: totalRecords,
          totalPages: Math.ceil(totalRecords / limit),
          data: categoryData,
        });
      }
    }
  );
};

module.exports.createCategory = async (req, res) => {
  let { name, parentId, description } = req.body;
  typeCategory = { parentId, description, name };
  let newCategory = new category(typeCategory);
  newCategory.save((err) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 500,
        message: Notification.ERRORDATABASE,
      });
    } else {
      res.json({
        statusCode: 200,
        message: Notification.CREATECATEGORYSUCCESS,
      });
    }
  });
};

module.exports.getCategoryByIdChildren = (req, res) => {
  let { categoryId } = req.params;
  category.find({ parentId: categoryId }, (err, dataCategory) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 500,
        message: Notification.ERRORDATABASE,
      });
    } else {
      res.json({
        statusCode: 200,
        data: dataCategory,
      });
    }
  });
};

module.exports.getCategoryById = (req, res) => {
    let { categoryId } = req.params;
    category.find({ _id: categoryId }, (err, dataCategory) => {
      if (err) {
        console.log(err);
        res.json({
          statusCode: 500,
          message: Notification.ERRORDATABASE,
        });
      } else {
        res.json({
          statusCode: 200,
          data: dataCategory,
        });
      }
    });
  };
  