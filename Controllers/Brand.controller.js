const path = require("path");
const multer = require("multer");
const fs = require("fs");

var brands = require("../Models/Brands.model");
const Notification = require("../Notification/Notification");

module.exports.getBrandsAll = (req, res) => {
  brands.find((err, brands) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 500,
        message: Notification.ERRORDATABASE,
      });
    }
    else{
       res.json({
          data: brands
       })
    }
  });
};

module.exports.getBrandsLimit = async (req, res) => {
  const limit = parseInt(req.query.limit) || 10;
  const page = parseInt(req.query.page) || 1;
  await brands.aggregate(
    [
      {
        $skip: (page - 1) * limit,
      },
      {
        $limit: limit,
      },
    ],
    async (err, brandData) => {
      const totalRecords = await brands.estimatedDocumentCount();
      if (err) {
        console.log(err);
        res.json({
          statusCode: 500,
          message: Notification.ERRORDATABASE,
        });
      } else {
        res.json({
          page: page,
          limit: limit,
          totalRecords: totalRecords,
          totalPages: Math.ceil(totalRecords / limit),
          data: brandData,
        });
      }
    }
  );
};

const storageBrand = multer.diskStorage({
  destination: "./Public/Uploads/BrandImage/",
  filename: function (req, file, cb) {
    cb(null, "IMAGE-BRAND" + Date.now() + path.extname(file.originalname));
  },
});
const uploadBrandImage = multer({
  storage: storageBrand,
  limits: { fileSize: 1000000 },
}).single("file");

module.exports.createBrand = async (req, res) => {
  await uploadBrandImage(req, res, (err) => {
    if (err) {
      console.log(err);
      res.json({
        statusCode: 500,
        message: Notification.UPLOADIMAGEFAILED,
      });
    } else {
      let { name } = req.body;
      let brandImage = null;
      if (req.file) {
        brandImage = req.file.filename;
      }
      typeBrand = { name, brandImage };
      let newBrand = new brands(typeBrand);
      newBrand.save((err) => {
        if (err) {
          console.log(err);
          res.json({
            statusCode: 500,
            message: Notification.ERRORDATABASE,
          });
        } else {
          res.json({
            statusCode: 200,
            message: Notification.CREATEBRANDSUCCESS,
          });
        }
      });
    }
  });
};
