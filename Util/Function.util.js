module.exports.DeleteItemArray = (Array, item) => {
  let i = Array.indexOf(item);
  if (i != -1) {
    Array.splice(i, 1);
  }
};
