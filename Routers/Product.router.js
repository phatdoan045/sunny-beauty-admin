var express = require("express")
var router = express.Router();
const controller= require('../Controllers/Product.controller')

router.get("/",controller.getAllProduct);
router.get("/image=/:image",controller.getImageProduct);
router.post("/",controller.createProduct);
router.delete("/:id",controller.deleteProduct);
router.put("/",controller.EditProduct);




module.exports=router;
