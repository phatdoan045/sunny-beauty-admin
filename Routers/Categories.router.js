var express = require("express")
var router = express.Router();
const controller= require('../Controllers/Categories.controller')


router.get("/all",controller.getCategoryAll);
router.get("/",controller.getCategoryLimit);
router.post("/",controller.createCategory);
router.get("/:categoryId/children",controller.getCategoryByIdChildren);
router.get("/:categoryId",controller.getCategoryById);





module.exports=router;
