var express = require("express")
var router = express.Router();
const controller= require('../Controllers/Brand.controller')


router.get("/all",controller.getBrandsAll);
router.get("/",controller.getBrandsLimit);
router.post("/",controller.createBrand);
// router.delete("/:id",controller.deleteBrand);





module.exports=router;
