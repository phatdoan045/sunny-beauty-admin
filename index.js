var express = require("express");
const mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors')

const productRouter = require("./Routers/Product.router");
const brandRouter = require("./Routers/Brand.router");
const categoryRouter = require("./Routers/Categories.router");

require('dotenv').config();

const Post = process.env.POST || 3001;
mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  },(err)=>{
      if(err) console.log(err + "");
      console.log("Connect Database Success");
  });


var app = express();
app.use(cors());
app.use([
    bodyParser.json(),
    bodyParser.urlencoded({
      extended: true,
    })
  ]);
app.use(express.static("Public"));

app.use('/product',productRouter);
app.use('/brands',brandRouter);
app.use('/categories',categoryRouter);


app.listen(Post,(err)=>{
    if(err) console.log(err);
    console.log(`Listen to ${Post}`);    
});
